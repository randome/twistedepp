# README #

Simple EPP server written with python using [Twisted networking library](https://twistedmatrix.com/trac/)

Main use case will be for testing your epp clients against a standard EPP implantation
See [RFC 5730](https://tools.ietf.org/html/rfc5730)

Could be expanded to become a full functioning EPP server solution.

TODO  :

* SSL support
* Redis for domain avail cache checks
* Some NoSQL for contact data persistence 
* Polling implementation
* Figure out how to handle domain expirations/deletions etc.
* And eventually move to python3 if possible

### How do I get set up? ###

`pip install -r requirements.txt`

`python server.py`