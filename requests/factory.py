def getCommand(command_name):
    # TODO : instantiate commands at __init__
    # then just return same instances

    if command_name == 'poll':
        from . import poll
        return poll.Poll

    if command_name == 'login':
        from . import login
        return login.Login
