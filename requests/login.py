from .request import Request
from templates import responses

class Login(Request):

    def __init__(self, soup):
        Request.__init__(self, soup)

    def process(self):
        username = self.soup.find("clID").text
        password = self.soup.find("pw").text
        print("login with", username, password)
        return responses.ok
