from .request import Request
from templates import responses

class Poll(Request):

    def __init__(self, soup):
        Request.__init__(self, soup)

    def process(self):
        command = self.soup.find("command").findNext()
        poll_op = command['op'] if command.has_attr("op") else ""

        if poll_op == 'req':
            print("Send poll rsp")
            return responses.poll_ack_message % dict(cl_trid=self.cl_trid, sv_trid=self.sv_trid)

        if poll_op == 'ack':
            msgID_to_ack = command['msgID'] if command.has_attr("msgID") else ""
            print("Ack msgid %s", msgID_to_ack)
            return responses.ok % dict(cl_trid=self.cl_trid)

        return responses.error
