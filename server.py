import struct

from bs4 import BeautifulSoup
from twisted.internet import protocol, reactor, endpoints

from templates.responses import ok, greeting, poll_ack_message, poll_no_messages

from requests import factory

class EppProtocol(protocol.Protocol):

    delimiter = "\r\n"

    def format_32(self):
        # Get the size of C integers. We need 32 bits unsigned.
        format_32 = ">I"
        if struct.calcsize(format_32) < 4:
            format_32 = ">L"
            if struct.calcsize(format_32) != 4:
                raise Exception("Cannot find a 32 bits integer")
        elif struct.calcsize(format_32) > 4:
            format_32 = ">H"
            if struct.calcsize(format_32) != 4:
                raise Exception("Cannot find a 32 bits integer")
        else:
            pass
        return format_32

    def pack(self, data):
        return struct.pack(self.format_32(), data)

    def unpack(self, data):
        return struct.unpack(self.format_32(), data)[0]

    def connectionMade(self):
        print("Connected from %s" % self.transport.getPeer())
        print("Sending greeting")
        self.sendResponse(greeting)

    def readHeader(self, data):
        header = []
        i = 0

        for line in data:
            header.append(line)
            if(i == 3):
               break
            i = i + 1

        header = ''.join(header) # \x16\x03\x01\x02
        package_length = self.unpack(header) - 4 # \x00\x00\x01x = 372 + 4 bytes
        print("Header, content length", package_length)

    def dataReceived(self, data):
        self.readHeader(data)
        body = data[4:]
        soup = BeautifulSoup(body, 'xml')

        command_tag = soup.find("command").findNext()
        command_name = command_tag.name
        CommandClass = factory.getCommand(command_name)

        command = CommandClass(soup)
        response = command.process()

        self.sendResponse(response)

    def sendResponse(self, data):
        length = self.pack(len(data) + 4)
        self.transport.write(length)
        # TODO : log sent responses
        self.transport.write(data.encode("utf-8"))

class EppFactory(protocol.Factory):
    def buildProtocol(self, addr):
        return EppProtocol()

print("Starting EPP server on port 1234")
endpoints.serverFromString(reactor, "tcp:1234").listen(EppFactory())
reactor.run()
