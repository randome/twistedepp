ok = """
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
   <epp xmlns="urn:ietf:params:xml:ns:epp-1.0">
     <response>
       <result code="1000">
         <msg>Command completed successfully</msg>
       </result>
       <trID>
         <clTRID>%(cl_trid)s</clTRID>
         <svTRID>54322-XYZ</svTRID>
       </trID>
     </response>
   </epp>
"""

greeting = """
<?xml version="1.0" encoding="UTF-8"?>
    <epp xmlns="urn:ietf:params:xml:ns:epp-1.0">
        <greeting>
            <svID>EPP Example Server</svID>
            <svDate>2014-10-24T09:22:07.973Z</svDate>
            <svcMenu>
                <version>1.0</version>
                <lang>en</lang>
                <domainURI>urn:ietf:params:xml:ns:domain-1.0</domainURI>
                <domainURI>urn:ietf:params:xml:ns:contact-1.0</domainURI>
                <domainURI>urn:ietf:params:xml:ns:host-1.0</domainURI>
                <svcExtension>
                    <extURI>urn:ietf:params:xml:ns:secDNS-1.1</extURI>
                </svcExtension>
            </svcMenu>
            <dcp>
                <access>
                    <none/>
                </access>
                <statement>
                    <purpose>
                        <prov/>
                    </purpose>
                    <recipient>
                        <ours/>
                    </recipient>
                    <retention>
                        <indefinite/>
                    </retention>
                </statement>
            </dcp>
        </greeting>
    </epp>
"""

poll_no_messages = """
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<epp xmlns="urn:ietf:params:xml:ns:epp-1.0">
  <response>
    <result code="1300">
      <msg>Command completed successfully; no messages</msg>
    </result>
    <trID>
      <clTRID>%(handle)s</clTRID>
      <svTRID>54321-XYZ</svTRID>
    </trID>
  </response>
</epp>
"""

poll_ack_message = """
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<epp xmlns="urn:ietf:params:xml:ns:epp-1.0">
  <response>
    <result code="1301">
      <msg>Command completed successfully; ack to dequeue</msg>
    </result>
    <msgQ count="1" id="12345">
      <qDate>2000-06-08T22:00:00.0Z</qDate>
      <msg>Transfer requested.</msg>
    </msgQ>
    <resData>
      <domain:trnData
       xmlns:domain="urn:ietf:params:xml:ns:domain-1.0">
        <domain:name>example.com</domain:name>
        <domain:trStatus>pending</domain:trStatus>
        <domain:reID>ClientX</domain:reID>
        <domain:reDate>2000-06-08T22:00:00.0Z</domain:reDate>
        <domain:acID>ClientY</domain:acID>
        <domain:acDate>2000-06-13T22:00:00.0Z</domain:acDate>
        <domain:exDate>2002-09-08T22:00:00.0Z</domain:exDate>
      </domain:trnData>
    </resData>
    <trID>
      <clTRID>%(cl_trid)s</clTRID>
      <svTRID>%(sv_trid)s</svTRID>
    </trID>
  </response>
</epp>
"""

error = """
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
   <epp xmlns="urn:ietf:params:xml:ns:epp-1.0">
     <response>
       <result code="2000">
         <msg>Error message</msg>
       </result>
       <trID>
         <clTRID>%(cl_trid)s</clTRID>
         <svTRID>54322-XYZ</svTRID>
       </trID>
     </response>
   </epp>
"""
